package com.zuitt.wdc0043;

import java.util.Scanner;

public class User {
    public static void main(String[] args){
        Scanner myobj1= new Scanner(System.in);
        System.out.println("Student's Information");
        System.out.println("Enter your first name:");
        String firstName = myobj1.nextLine();

        System.out.println("Enter your last name:");
        String lastName = myobj1.nextLine();

        System.out.println("Enter your first subject grade:");
        double firstSubject = myobj1.nextDouble();

        System.out.println("Enter your second subject grade:");
        double secondSubject = myobj1.nextDouble();

        System.out.println("Enter your third subject grade:");
        double thirdSubject = myobj1.nextDouble();

        System.out.println("First Name:");
        System.out.println(firstName);

        System.out.println("Last Name:");
        System.out.println(lastName);

        System.out.println("First Subject Grade:");
        System.out.println(firstSubject);

        System.out.println("Second Subject Grade:");
        System.out.println(secondSubject);

        System.out.println("Third Subject Grade:");
        System.out.println(thirdSubject);

        double average = ((firstSubject+secondSubject+thirdSubject)/3);
        System.out.println("Good day, "+ firstName +" " + lastName+".");
        System.out.println("Your grade average is: "+ average);

    }
}
